#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
from functools import partial, reduce
from typing import TypeVar, Any, Union, Optional, Callable

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from matplotlib.markers import MarkerStyle
from matplotlib.lines import Line2D
import seaborn as sb  # type: ignore


"""
Plot facets on a Matplotlib figure using `constrained_layout`.

The goal of this small library is to generalize the hierarchical plotting
functions provided in seaborn using a more functional interface.
One creates a Mapper object with defined (optional) rows, columns and hues
variables, and then one can freely iterate on those variables and
the corresponding subplots.

It also allows plotting a figure-level legend with its own space.

The use of Matplotlib's `constrained_layout` allows for nice layouts
for complex figures.
"""


__all__ = [
  "hierarchy",
  "Mapper",
]


T = TypeVar("T")
T2 = TypeVar("T2")
Tree = Optional[Union[list[str], dict[str, "Tree"]]]

logger = logging.getLogger(__name__)

# All levels, in dependency order
hierarchy = [
  "row",
  "column",
  "color",
  "linestyle",
  "marker",
  "alpha",
  "linewidth",
  "markersize",
]


def axes_levels(levels: list[str]):
  """Levels used *inside* an axes."""
  return [lvl for lvl in levels if lvl not in ["row", "column"]]


def values(xs, deps: Optional[list[str]] = None, **kw):
  """Values for some level, given the values for the levels it depends on."""
  if deps is not None:
    for lv in deps:
      try:
        v = kw[lv + "v"]
      except KeyError:
        # Is TypeError the right exception to raise here ?
        # it means the type of the function is dependent on the value of deps,
        # which is ugly...
        raise TypeError(f"required argument '{lv}v'")
      xs = xs[v]
    return xs
  else:
    return xs


def unique_leaf_values(xs: Tree):
  """Return all the values from the list or nested dict `xs`."""
  if xs is None:
    return []
  elif isinstance(xs, list):
    # remove duplicates
    # don't use `set(.)` as it does not maintain order
    return list(dict.fromkeys(xs))
  elif isinstance(xs, dict):
    return reduce(
      lambda x1, x2: list(dict.fromkeys(x1 + x2)),
      [unique_leaf_values(x) for x in xs.values()],
    )
  else:
    logger.error(f"unique_leaf_values not implemented for xs of type {type(xs)}: {xs}")
    raise NotImplementedError


def nrows(rows: Optional[list[T]]):
  """Number of rows needed for the semantic row values `rows`."""
  if rows is None:
    return 1
  elif isinstance(rows, list):
    return len(rows)
  else:
    raise TypeError("expected 'rows' to be a list or None")


def ncolumns(cols: Union[None, list[T], dict[T2, list[T]]]) -> int:
  """Number of columns needed for the semantic column values `cols`."""
  if cols is None:
    return 1
  elif isinstance(cols, list):
    return len(cols)
  elif isinstance(cols, dict):
    return max([len(cs) for cs in cols.values()])
  else:
    raise TypeError("expected 'cols' to be a list, a dict or None")


def validate_deps(deps: Optional[dict[str, list[str]]], levels: list[str] = hierarchy):
  """Validate dependencies `deps` between levels `levels`, and fill missing."""
  if deps is None:
    # All levels are independent, that's ok
    deps = dict()

  for i, lvl in enumerate(levels):
    try:
      # find this level's deps
      lvl_deps = deps[lvl]
    except KeyError:
      # no deps, that's ok
      deps[lvl] = []
    else:
      # it's ok to depend on previous set levels in the hierarchy only
      ok_levels = levels[:i]
      bad_levels = [lv for lv in lvl_deps if lv not in ok_levels]
      if bad_levels:
        # not empty
        raise ValueError(
          f"'{lvl}' level cannot depend on {bad_levels} levels, "
          f"only on {ok_levels}, the previous levels that are set."
        )

  return deps


def reciter(f, levels: list[str], get_values, **kw):
  """Call f for every combination of values of `levels`."""
  lvl = levels[0]
  vs = get_values(lvl, **kw)
  for v in vs:
    # call f with lvl=v
    kw = {**kw, **{lvl: v}}
    if len(levels) == 1:
      # no remaining levels to iterate over
      # call f with correct args
      f(**kw)
    else:
      # iterate on next level
      reciter(f, levels[1:], get_values, **kw)


def set_default_levelmaps(
  levelmaps: dict[str, list[Any]], levels: dict[str, list[str]]
):
  """Fill in default values for level maps."""
  for k, d in levelmaps.items():
    lvl = levels[k]
    if d is None and lvl is not None:
      vs = unique_leaf_values(lvl)
      n = len(vs) if vs != [] else 1
      if k == "color":
        xs = sb.color_palette(n_colors=n)
      elif k == "linestyle":
        xs = list(Line2D.lineStyles.keys())[:n]
      elif k == "marker":
        xs = list(MarkerStyle.markers.keys())[:n]
      elif k == "alpha":
        xs = np.arange(start=1.0, stop=0.0, step=-1 / n)[:-1]
      elif k == "linewidth":
        # FIXME improve
        xs = range(n, 0, -1)
      elif k == "markersize":
        # FIXME improve
        xs = range(n, 0, -1)
      else:
        raise ValueError("level not recognized")
      levelmaps[k] = {v: x for v, x in zip(vs, xs)}


def empty_handle() -> Patch:
  return Patch(alpha=0.0)


def flatten(ll: list[list[T]]):
  return [x for li in ll for x in li]


def legend_columns(
  handlabs: dict[str, list[Any]],
  col_len: Callable[[int], int],
  relabel: Optional[Callable[[str], str]] = None,
):
  if relabel is None:

    def relabel(s):
      return s

  # `1 + ...` for the subtitle
  lens = {lv: 1 + len(li) for lv, li in handlabs.items()}
  max_len = max(lens.values())
  clen = col_len(max_len)

  def subtitle(lv):
    return (empty_handle(), "$\\bf{" + relabel(lv) + "}$")

  def complete_column(
    cur_col: list[tuple[Patch, str]], cur_len: int, cols: list[list[tuple[Patch, str]]]
  ):
    cur_col.extend((clen - cur_len) * [(empty_handle(), "")])
    cols.append(cur_col)
    cur_col = list()
    cur_len = 0
    return cur_col, cur_len, cols

  def ceil_div(n, m):
    k = n // m
    if n % m > 0:
      return k + 1
    else:
      return k

  cols: list[list[tuple[Patch, str]]] = list()
  cur_col: list[tuple[Patch, str]] = list()
  cur_len = 0
  for lv, hls in handlabs.items():
    # use current column or add new column, or split level
    if lens[lv] > clen:
      # the level is too long, split it into multiple columns
      # and start on a new column
      if cur_len > 0:
        cur_col, cur_len, cols = complete_column(cur_col, cur_len, cols)
      # TODO we could consider leaving a blank space in the subtitle space
      # for the subsequent columns, but that changes the calculations
      hls = [subtitle(lv)] + hls
      for i in range(ceil_div(lens[lv], clen)):
        cur_col.extend(hls[i * clen : (i + 1) * clen])
        cur_col, cur_len, cols = complete_column(cur_col, cur_len, cols)
    else:
      if lens[lv] <= clen - cur_len:
        # fits in current column, so use it
        cur_col.extend([subtitle(lv)] + hls)
        cur_len += lens[lv]
      else:
        # need a new column, first pad the current column
        cur_col, cur_len, cols = complete_column(cur_col, cur_len, cols)
        cur_col.extend([subtitle(lv)] + hls)
        cur_len += lens[lv]

  if cur_len > 0:
    # unfinished column
    _, _, cols = complete_column(cur_col, cur_len, cols)

  return cols


def horizontal_legend_columns(handlabs, relabel):
  def col_len(max_len):
    return min(max_len, 5)

  return legend_columns(handlabs=handlabs, col_len=col_len, relabel=relabel)


def vertical_legend_columns(handlabs, relabel):
  def col_len(max_len):
    return max(max_len, 10)

  return legend_columns(handlabs=handlabs, col_len=col_len, relabel=relabel)


class Mapper(object):
  """
  A Mapper maps semantic values to levels on a figure.

  The hierarchical levels available are `rows`, `columns`,
  `colors`, `linestyles`, `markers`, `linewidths` and `markersizes`.
  A level can depend only on the previous ones, so for instance,
  the colors used in a facet can depend on the column,
  but not on the linestyle.
  """

  def __init__(
    self,
    deps=None,
    rows=None,
    columns=None,
    colors=None,
    linestyles=None,
    markers=None,
    alphas=None,
    linewidths=None,
    markersizes=None,
    palette=None,
    stylemap=None,
    markermap=None,
    alphamap=None,
    linewidthmap=None,
    markersizemap=None,
    legend=None,
    legend_layout=None,
    legend_title=None,
    label_legend=None,
    sharex=None,
    sharey=None,
    aspect=None,
    figsize=None,
  ):
    """Set up the figure, its layout and its subplots."""
    self.levels = {
      "row": rows,
      "column": columns,
      "color": colors,
      "linestyle": linestyles,
      "marker": markers,
      "alpha": alphas,
      "linewidth": linewidths,
      "markersize": markersizes,
    }

    self.used_levels = [lvl for lvl in hierarchy if self.levels[lvl] is not None]
    self.deps = validate_deps(deps, self.used_levels)

    self.levelmaps = {
      "color": palette,
      "linestyle": stylemap,
      "marker": markermap,
      "alpha": alphamap,
      "linewidth": linewidthmap,
      "markersize": markersizemap,
    }
    set_default_levelmaps(self.levelmaps, self.levels)

    self.nrows = nrows(rows)
    self.ncols = ncolumns(columns)

    if aspect:
      subplot_kw = {"aspect": aspect}
    else:
      subplot_kw = {}

    f = plt.figure(
      constrained_layout=True,
      figsize=figsize,
    )
    self.fig = f

    self.spec = self.set_legend_grid(
      legend, layout=legend_layout, title=legend_title, relabel=label_legend
    )

    # improve constrained layout
    f.set_constrained_layout_pads(
      w_pad=0,
      h_pad=0,
      wspace=0.05,
      hspace=0.05,
    )

    self.add_subplots(sharex, sharey, subplot_kw)

  def set_legend_grid(self, legend, layout, title, relabel):
    if layout is None or layout == "by_level":
      handlabs = self.legend_entries_by_level(relabel=relabel)
    elif layout == "by_element":
      handlabs = self.legend_entries_by_element(relabel=relabel)
    else:
      raise ValueError("invalid legend_layout value")
    # FIXME to decide the number of columns,
    # also pass the figure size to determine the space available
    if legend == "horizontal":
      columns = horizontal_legend_columns(handlabs, relabel=relabel)
      ncols = len(columns)
      handlabs = flatten(columns)
      # FIXME not sure that this is correctly implemented...
      # can't we pass it as a title directly ?
      if title:
        handlabs = [(empty_handle(), title)] + handlabs
      handles, labels = zip(*handlabs)
      # get legend size
      lw, lh = self.get_legend_size_inches(
        handles=handles,
        labels=labels,
        ncols=ncols,
      )

      # make a grid to partition between plots and legend
      w, h = self.fig.get_size_inches()
      needed = lh / h + 0.01
      logger.debug("Space needed:", needed)
      g_all = self.fig.add_gridspec(2, 1, height_ratios=[1 - needed, needed])
      # put the plots into the first row
      g = g_all[0].subgridspec(self.nrows, self.ncols)
      # put the legend into the second row
      ax_legend = self.fig.add_subplot(g_all[1])
      # hide ax_legend
      ax_legend.axis("off")
      self.legend = ax_legend.legend(
        handles=handles,
        labels=labels,
        ncols=ncols,
        frameon=False,
      )

      # return grid for plots
      return g
    elif legend == "vertical":
      columns = vertical_legend_columns(handlabs, relabel=relabel)
      ncols = len(columns)
      handlabs = flatten(columns)
      handles, labels = zip(*handlabs)
      # get legend size
      lw, lh = self.get_legend_size_inches(
        handles=handles,
        labels=labels,
        ncols=ncols,
        title=title,
      )

      # make a grid to partition between plots and legend
      w, h = self.fig.get_size_inches()
      needed = lw / w + 0.01
      logger.debug("Space needed:", needed)
      g_all = self.fig.add_gridspec(1, 2, width_ratios=[1 - needed, needed])
      # put the plots into the first column
      g = g_all[0].subgridspec(self.nrows, self.ncols)
      # put the legend into the second column
      ax_legend = self.fig.add_subplot(g_all[1])
      # hide ax_legend
      ax_legend.axis("off")
      self.legend = ax_legend.legend(
        handles=handles,
        labels=labels,
        ncols=ncols,
        title=title,
        frameon=False,
      )

      # return grid for plots
      return g
    elif legend is None or not legend:
      return self.fig.add_gridspec(self.nrows, self.ncols)
    else:
      raise ValueError("legend not 'vertical', 'horizontal', False, or None")

  def shared_with(self, axes, row, col, share):
    if isinstance(share, np.ndarray):
      i, j = share[row, col]
      return axes[i, j]
    elif share is None:
      return None
    elif not share:
      return None
    elif share == "all":
      return axes[0, 0]
    elif share == "row":
      return axes[row, 0]
    elif share == "col":
      return axes[0, col]
    else:
      raise ValueError("Invalid value for share")

  def level_values(self, level, **kw):
    try:
      deps = self.deps[level]
    except KeyError:
      deps = None
    xs = self.levels[level]
    if xs is None:
      return [None]
    else:
      return values(self.levels[level], deps=deps, **kw)

  def add_subplots(self, sharex, sharey, subplot_kw):
    # copied from Figure.subplots
    axes = np.empty((self.nrows, self.ncols), dtype=object)
    for row in range(self.nrows):
      for col in range(self.ncols):
        subplot_kw["sharex"] = self.shared_with(axes, row, col, sharex)
        subplot_kw["sharey"] = self.shared_with(axes, row, col, sharey)
        axes[row, col] = self.fig.add_subplot(self.spec[row, col], **subplot_kw)

    # turn off redundant tick labeling
    if sharex in ["col", "all"]:
      # turn off all but the bottom row
      for ax in axes[:-1, :].flat:
        ax.xaxis.set_tick_params(which="both", labelbottom=False, labeltop=False)
        ax.xaxis.offsetText.set_visible(False)

    # I cheat with the isinstance, for the more complex case
    # for instance used in cases_and_prms.py
    if isinstance(sharey, np.ndarray) or sharey in ["row", "all"]:
      # turn off all but the first column
      for ax in axes[:, 1:].flat:
        ax.yaxis.set_tick_params(which="both", labelleft=False, labelright=False)
        ax.yaxis.offsetText.set_visible(False)

    # despine all axes (top and right)
    for side in ["top", "right"]:
      for ax in axes[:, :].flat:
        ax.spines[side].set_visible(False)

    self.fig.align_ylabels(axs=axes[:, 0])

    # set up the axes relations
    # need to flip the axes because of the deps
    df_axes = pd.DataFrame(
      {
        rowv: pd.Series(
          {
            columnv: axes[i, j]
            for (j, columnv) in enumerate(self.level_values("column", rowv=rowv))
          }
        )
        for (i, rowv) in enumerate(self.level_values("row"))
      }
    )

    self.axes = df_axes

  def get_legend_size_inches(self, **kwargs):
    legend = self.fig.legend(**kwargs)
    lwe = legend.get_window_extent(self.fig.canvas.get_renderer())
    lw = lwe.width / self.fig.dpi
    lh = lwe.height / self.fig.dpi
    legend.remove()

    return (lw, lh)

  def get_ax(self, row, column):
    return self.axes.loc[column, row]

  def add_level_repr(self, level, kw):
    try:
      v = kw[level + "v"]
    except KeyError:
      pass
    else:
      kw[level] = self.levelmaps[level][v]

      return kw

  def call_with_maps(self, f, levels, **kw):
    for lvl in levels:
      kw = self.add_level_repr(lvl, kw)

    f(**kw)

  def call_with_ax(self, f, **kw):
    try:
      row = kw["rowv"]
    except KeyError:
      row = None
    try:
      column = kw["columnv"]
    except KeyError:
      column = None

    ax = self.axes.loc[column, row]
    f(ax=ax, **kw)

  def sub_levels(self, levels=None):
    if levels is None:
      levels = self.used_levels
    else:
      # put in order and remove bad levels
      levels = [lvl for lvl in self.used_levels if lvl in levels]

    return levels

  def iter(self, f, levels=None):
    """Call `f` for every combination of level values."""
    levels = self.sub_levels(levels)

    levelvs = [lvl + "v" for lvl in levels]

    def get_values(lvl, **kw):
      return self.level_values(lvl[:-1], **kw)

    def g(**kw):
      self.call_with_maps(
        f=partial(self.call_with_ax, f=f), levels=axes_levels(levels), **kw
      )

    reciter(
      g,
      levels=levelvs,
      get_values=get_values,
    )

  def iter_columns(self, f):
    """Call `f` for each column."""
    for columnv in self.level_values("column"):
      f(axes=self.axes.loc[columnv, :], columnv=columnv)

  def iter_rows(self, f):
    """Call `f` for each row."""
    for rowv in self.level_values("row"):
      f(axes=self.axes.loc[:, rowv], rowv=rowv)

  def iter_column_rows(self, f, columnv):
    """Call `f` for each subplot in `column`."""
    for rowv in self.level_values("row"):
      f(ax=self.axes.loc[columnv, rowv], rowv=rowv)

  def iter_row_columns(self, f, rowv):
    """Call `f` for each subplot in `row`."""
    for columnv in self.level_values("column", rowv=rowv):
      f(ax=self.axes.loc[columnv, rowv], columnv=columnv)

  def legend_entries_by_level(self, levels=None, relabel=None):
    """Dict of legend entries optionally relabeled using function `relabel` by level."""

    # legend labels
    def lab(c):
      if relabel:
        return relabel(c)
      else:
        return c

    # color level
    def patch(c):
      return Patch(
        facecolor=c,
      )

    # linestyle level
    def line(ls):
      return Line2D(
        [0.0],
        [0.0],
        linestyle=ls,
        color="black",
      )

    # marker level
    def mark(mk):
      return Line2D(
        [0.0],
        [0.0],
        linestyle="",
        marker=mk,
        markerfacecolor="black",
      )

    # alpha level
    def alpha_patch(a):
      return Patch(alpha=a, color="black")

    # linewidth level
    def linew(w):
      return Line2D(
        [0.0],
        [0.0],
        linewidth=w,
        color="black",
      )

    # markersize level
    def point(sz):
      return Line2D(
        [0.0],
        [0.0],
        linestyle="",
        marker="o",
        markersize=sz,
        markerfacecolor=(1.0, 1.0, 1.0, 0.0),
        color="black",
      )

    # only keep valid levels, in order, and exclude 'row'/'column' levels for which we don't need a legend
    levels = axes_levels(self.sub_levels(levels))

    handles = {
      "color": patch,
      "linestyle": line,
      "marker": mark,
      "alpha": alpha_patch,
      "linewidth": linew,
      "markersize": point,
    }

    def handle(lvl, v):
      return handles[lvl](self.levelmaps[lvl][v])

    handlabs = {
      lvl: [(handle(lvl, v), lab(v)) for v in unique_leaf_values(self.levels[lvl])]
      for lvl in levels
    }

    return handlabs

  def legend_entries_by_element(self, levels=None, relabel=None):
    # legend labels
    def lab(*cs):
      if relabel:
        return relabel(*cs)
      else:
        return " | ".join([str(c) for c in cs])

    def rich_handle(
      color="black",
      linestyle="",
      linewidth=None,
      marker=None,
      markersize=None,
      alpha=None,
    ):
      return Line2D(
        [0.0],
        [0.0],
        color=color,
        linestyle=linestyle,
        linewidth=linewidth,
        marker=marker,
        markersize=markersize,
        markerfacecolor=color,
        alpha=alpha,
      )

    # only keep valid levels, in order, and exclude 'row'/'column' levels for which we don't need a legend
    levels = axes_levels(self.sub_levels(levels))
    levelvs = [lvl + "v" for lvl in levels]

    handlabs = list()

    def f(**kw):
      labs = [v for k, v in kw.items() if k.endswith("v")]
      lvs = {k: v for k, v in kw.items() if not k.endswith("v")}
      label = lab(*labs)
      handle = rich_handle(**lvs)
      handlabs.append((handle, label))

    def get_values(lvl, **kw):
      return self.level_values(lvl[:-1], **kw)

    def g(**kw):
      self.call_with_maps(f=f, levels=levels, **kw)

    reciter(g, levels=levelvs, get_values=get_values)

    return {"": handlabs}

  def _redraw(self):
    self.fig.draw(self.fig.canvas.get_renderer())

  def set_facet_titles(self, f, **kwargs):
    def g(ax, rowv, columnv):
      title = f(rowv=rowv, columnv=columnv)
      ax.set_title(title, **kwargs)

    self.iter(g, levels=["row", "column"])

  def set_row_titles(self, f=lambda x: x, **kwargs):
    def g(axes, rowv):
      title = f(rowv)
      axes.iloc[0].set_title(title, loc="left", **kwargs)

    self.iter_rows(g)

  def set_column_titles(self, f=lambda x: x, **kwargs):
    def g(axes, columnv):
      title = f(columnv)
      axes.iloc[0].set_title(title, loc="center", **kwargs)

    self.iter_columns(g)

  def savefig(self, fname, **kwargs):
    self.fig.savefig(fname, **kwargs)

  def closefig(self):
    plt.close(fig=self.fig)
