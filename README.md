Gridplot
========

Plot facets on a Matplotlib figure using `constrained_layout`.

The goal of this small library is to generalize the hierarchical plotting
functions provided in seaborn using a more functional interface.
One creates a Mapper object with defined (optional) rows, columns and hues
variables, and then one can freely iterate on those variables and
the corresponding subplots.

It also allows plotting a figure-level legend with its own space.

The use of Matplotlib's `constrained_layout` allows for nice layouts
for complex figures.
